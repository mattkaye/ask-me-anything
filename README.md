# Ask Me Anything WordPress Plugin
This is a WordPress plugin which allows you to create modal window forms that collect question data from your users. The QA style feedback can be either private or posted to the site as a public thread. The form questions and styles are highly configurable.
Simply install the plugin and away you go!

This plugin is no longer under active development. An archived version of the WordPress hosted plugin page can be found [here](https://wordpress.org/plugins/answer-my-question/).

### Videos
![](https://png.icons8.com/circled-play/office/30/000000) [Styling custom form](http://138.197.95.142/matt-portfolio/ask-me-anything-1.webm)

![](https://png.icons8.com/circled-play/office/30/000000) [Adding questions to a form](http://138.197.95.142/matt-portfolio/ask-me-anything-2.webm)

### Screenshots
##### Question administration table:
[![alt text](https://ps.w.org/answer-my-question/trunk/screenshot-1.png?rev=1539064 "Question administration table")](https://ps.w.org/answer-my-question/trunk/screenshot-1.png?rev=1539064)

##### Answer the users question either on the website or directly via email:
[![alt text](https://ps.w.org/answer-my-question/trunk/screenshot-2.png?rev=1539064 "Answer the users question either on the website or directly via email")](https://ps.w.org/answer-my-question/trunk/screenshot-2.png?rev=1539064)

##### Modal window on client facing side of the site for the user to submit:
[![alt text](https://ps.w.org/answer-my-question/trunk/screenshot-3.png?rev=1539064 "Modal window on client facing side of the site for the user to submit")](https://ps.w.org/answer-my-question/trunk/screenshot-3.png?rev=1539064)